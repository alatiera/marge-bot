FROM registry.access.redhat.com/ubi8/python-36:1

RUN git clone --depth=1 https://github.com/smarkets/marge-bot marge-bot && \
    cd marge-bot && \
    pip install --no-cache-dir -r requirements_frozen.txt

ENTRYPOINT ["python", "/opt/app-root/src/marge-bot/marge.app"]
